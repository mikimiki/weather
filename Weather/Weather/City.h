//
//  City.h
//  TestWeatherApp
//
//  Created by Miki Dimitrov on 11/28/15.
//  Copyright © 2015 Miki Dimitrov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface City : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *temperature;
@property (nonatomic, copy) NSString *humidity;
@property (nonatomic, copy) NSString *desc;


- (instancetype)initWithCityname:(NSString *)name
                     temperature:(NSString *)temperature
                        humidity:(NSString *)humidity
                            desc:(NSString *)desc;
@end
