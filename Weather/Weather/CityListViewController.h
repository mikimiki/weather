//
//  ViewController.h
//  Weather
//
//  Created by Miki Dimitrov on 11/30/15.
//  Copyright © 2015 PovioLabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CityTableViewCell.h"
#import "AddCityTableViewCell.h"
#import "DataModel.h"
#import "City.h"


@interface CityListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,assign) NSUInteger currentIndexPath;
@property(nonatomic,retain)UIRefreshControl *refreshControl;


@end

