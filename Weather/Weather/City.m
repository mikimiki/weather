//
//  City.m
//  TestWeatherApp
//
//  Created by Miki Dimitrov on 11/28/15.
//  Copyright © 2015 Miki Dimitrov. All rights reserved.
//

#import "City.h"

@implementation City
#pragma mark - Object Lifecycle
- (instancetype)initWithCityname:(NSString *)name
                     temperature:(NSString *)temperature
                        humidity:(NSString *)humidity
                          desc:(NSString *)desc{
    self = [super init];
    if (self) {
        _name = name;
        _temperature = temperature;
        _humidity = humidity;
        _desc = desc;
    }
    return self;
}
@end
