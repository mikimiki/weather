//
//  APIClient.h
//  TestWeatherApp
//
//  Created by Miki Dimitrov on 11/28/15.
//  Copyright © 2015 Miki Dimitrov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import "Constants.h"
#import "Reachability.h"


typedef void (^APIClientCompletionBlock)(id object, NSError *error);

@interface APIClient : NSObject

@property (nonatomic) Reachability *internetReachability;

+(APIClient *) sharedClient;

- (void)getWeatherInformationForCity:(NSString *)city andCompletionBlock:(APIClientCompletionBlock)completionBlock;



@end
