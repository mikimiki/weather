//
//  AddCityTableViewCell.m
//  Weather
//
//  Created by Miki Dimitrov on 11/30/15.
//  Copyright © 2015 PovioLabs. All rights reserved.
//

#import "AddCityTableViewCell.h"

@implementation AddCityTableViewCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        NSArray *nibArray = [[NSBundle mainBundle]loadNibNamed:@"AddCityTableViewCell" owner:self options:nil];
        self = [nibArray objectAtIndex:0];
    }
    return self;
}
+ (NSString *)reuseIdentifier {
    return @"AddCityCustomCell";
}

@end
