//
//  DataModel.m
//  TestWeatherApp
//
//  Created by Miki Dimitrov on 11/28/15.
//  Copyright © 2015 Miki Dimitrov. All rights reserved.
//

#import "DataModel.h"
#import "Constants.h"
#import "City.h"

static DataModel *_model = nil;

@implementation DataModel
//--------------------------------------------------------------------------------------------------------------
+(DataModel*)model{
    @synchronized([DataModel class]){
        if (!_model) {
            _model = [[self alloc] init];
            _model.orderedCities = [[NSArray alloc]init];
            _model.choosenCities = [[NSMutableArray alloc]init];
            [_model getCitiesFromUserDefaults];
        }
        return _model;
    }
    return nil;
}
//--------------------------------------------------------------------------------------------------------------
-(void)parseLocalJson{
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"city.list" ofType:@"json"];
    NSString *myList = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
    NSMutableArray *randomSelection = [[NSMutableArray alloc]init];
    
    NSArray *myItems = [myList componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    for( NSString *JSON in myItems)
    {
        if( [JSON length]==0)
        {
            continue;
        }
        NSError *error;
        NSData *objectData = [JSON dataUsingEncoding:NSUTF8StringEncoding];
        id json = [NSJSONSerialization JSONObjectWithData:objectData options:0 error:nil];
        NSString *name =[json objectForKey:@"name"];
        [randomSelection addObject:name];
        
        if( json == nil )
        {
            NSLog( @"Error %@ reading\n%@", error, JSON);
        }
    }
    
    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:randomSelection];
    self.orderedCities = [orderedSet array];
}
//--------------------------------------------------------------------------------------------------------------
- (City*)parseCity:(NSDictionary*)json andCityName:(NSString*)cityName{
    City *currentCity = [[City alloc]initWithCityname:cityName temperature:[[json objectForKey:@"main"]objectForKey:@"temp"] humidity:[[json objectForKey:@"main"]objectForKey:@"humidity"] desc:[[[json objectForKey:@"weather"]objectAtIndex:0]objectForKey:@"description"]];
    return currentCity;
}
//--------------------------------------------------------------------------------------------------------------
- (void)addCity:(NSString*)cityName{
    [self.choosenCities addObject:cityName];
    [self saveCitiesToUserDefaults];
}
//--------------------------------------------------------------------------------------------------------------
- (void)removeCityWithName:(NSString*)cityName{
    NSInteger cityIndex = -1;
    for (NSString *city in self.choosenCities) {
        if ([cityName isEqualToString:city]) {
            cityIndex = [self.choosenCities indexOfObject:city];
            break;
        }
    }
    if (cityIndex != -1) {
        [self.choosenCities removeObjectAtIndex:cityIndex];
        [self saveCitiesToUserDefaults];
    }
}
//--------------------------------------------------------------------------------------------------------------
- (void)saveCitiesToUserDefaults{
    [[NSUserDefaults standardUserDefaults] setObject:self.choosenCities forKey:@"Cities"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
//--------------------------------------------------------------------------------------------------------------
- (void)getCitiesFromUserDefaults{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"Cities"]) {
        NSArray *array = [[NSUserDefaults standardUserDefaults] objectForKey:@"Cities"];
        self.choosenCities = [array mutableCopy];
    }
}

@end
