//
//  AppDelegate.h
//  Weather
//
//  Created by Miki Dimitrov on 11/30/15.
//  Copyright © 2015 PovioLabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataModel.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

