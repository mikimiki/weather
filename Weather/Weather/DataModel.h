//
//  DataModel.h
//  TestWeatherApp
//
//  Created by Miki Dimitrov on 11/28/15.
//  Copyright © 2015 Miki Dimitrov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIClient.h"
#import "City.h"

@interface DataModel : NSObject
+(DataModel*)model;

//Cities
-(void)parseLocalJson;

@property (nonatomic,retain)NSArray *orderedCities;
@property(nonatomic,retain)NSMutableArray *choosenCities;
@property (nonatomic,assign)BOOL fetchInProgress;


- (City*)parseCity:(NSDictionary*)json andCityName:(NSString*)cityName;
- (void)addCity:(NSString*)cityName;
- (void)removeCityWithName:(NSString*)cityName;
- (void)saveCitiesToUserDefaults;
- (void)getCitiesFromUserDefaults;

@end
