//
//  WeatherInformationViewController.m
//  Weather
//
//  Created by Miki Dimitrov on 11/30/15.
//  Copyright © 2015 PovioLabs. All rights reserved.
//

#import "WeatherInformationViewController.h"

@interface WeatherInformationViewController ()

@end

@implementation WeatherInformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.lblCityName.text = self.city.name;
    double temp = [self.city.temperature doubleValue];
    int celsius = temp-273.15;
    self.lblTemperature.text = [NSString stringWithFormat:@"%d%@C",celsius,@"\u00B0"];
    NSLog(@"%@",self.city.humidity);
    self.lblHumidity.text = [NSString stringWithFormat:@"Humidity: %@",self.city.humidity];
    self.lblDescription.text = self.city.desc;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
