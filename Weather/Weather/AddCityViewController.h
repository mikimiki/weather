//
//  AddCityViewController.h
//  Weather
//
//  Created by Miki Dimitrov on 11/30/15.
//  Copyright © 2015 PovioLabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APIClient.h"
#import "DataModel.h"
#import "City.h"

@interface AddCityViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UISearchController *searchController;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@property(nonatomic,copy) NSArray *filteredCity;


@end
