//
//  AddCityViewController.m
//  Weather
//
//  Created by Miki Dimitrov on 11/30/15.
//  Copyright © 2015 PovioLabs. All rights reserved.
//

#import "AddCityViewController.h"

@interface AddCityViewController ()

@end

@implementation AddCityViewController
//--------------------------------------------------------------------------------------------------------------
- (void)viewDidLoad {
    [super viewDidLoad];
    self.searchBar.delegate = self;
      self.searchBar.autocapitalizationType = UITextAutocapitalizationTypeAllCharacters;
    self.tableView.hidden = YES;
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
    // Do any additional setup after loading the view.
}
//--------------------------------------------------------------------------------------------------------------
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.searchBar becomeFirstResponder];
}
//--------------------------------------------------------------------------------------------------------------
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//--------------------------------------------------------------------------------------------------------------
#pragma mark TableView-Delegate
//--------------------------------------------------------------------------------------------------------------
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.filteredCity count];
}
//--------------------------------------------------------------------------------------------------------------
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.textLabel.text =[self.filteredCity objectAtIndex:indexPath.row];
    return cell;
}
//--------------------------------------------------------------------------------------------------------------
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cityName = [self.filteredCity objectAtIndex:indexPath.row];
    [[DataModel model] addCity:cityName];
    [self.navigationController popViewControllerAnimated:YES];
}
//--------------------------------------------------------------------------------------------------------------
#pragma mark SearchBar-Delegate
//--------------------------------------------------------------------------------------------------------------
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@",searchText];
        self.filteredCity = [[DataModel model].orderedCities filteredArrayUsingPredicate:resultPredicate];
 
}
//--------------------------------------------------------------------------------------------------------------
-(BOOL)searchDisplayController:(UISearchBar *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    
    [self filterContentForSearchText:searchString
                               scope:[[self.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchBar
                                                     selectedScopeButtonIndex]]];
    return YES;
}
//--------------------------------------------------------------------------------------------------------------
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{

    if (searchBar.text.length == 0) {
        self.tableView.hidden = YES;
    }else{
        self.tableView.hidden = NO;
    }
}
//--------------------------------------------------------------------------------------------------------------
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    self.tableView.hidden = YES;
    [self.navigationController popViewControllerAnimated:YES];
}
//--------------------------------------------------------------------------------------------------------------




@end
