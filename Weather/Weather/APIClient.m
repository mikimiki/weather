//
//  APIClient.m
//  TestWeatherApp
//
//  Created by Miki Dimitrov on 11/28/15.
//  Copyright © 2015 Miki Dimitrov. All rights reserved.
//

#import "APIClient.h"
@interface APIClient ()
@property (nonatomic, assign) BOOL hasInternetConnection;
@end

static APIClient *_sharedClient = nil;

@implementation APIClient
+(APIClient*)sharedClient{
    @synchronized([APIClient class]){
        if (!_sharedClient) {
            _sharedClient = [[self alloc] init];
            _sharedClient.internetReachability = [Reachability reachabilityForInternetConnection];
            [_sharedClient.internetReachability startNotifier];
           [_sharedClient updateInterfaceWithReachability:_sharedClient.internetReachability];
        }
        return _sharedClient;
    }
    return nil;
}

+(id)alloc{
    @synchronized([APIClient class]){
        NSAssert(_sharedClient==nil, @"Attempted to allocate a second instance of a singleton");
        _sharedClient = [super alloc];
        return _sharedClient;
    }
    return nil;
}
//--------------------------------------------------------------------------------------------------------------
- (void)getWeatherInformationForCity:(NSString *)city andCompletionBlock:(APIClientCompletionBlock)completionBlock{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    NSString *weatherUrl = [NSString stringWithFormat:kWeatherKeyUrl,city];
    NSString *cityInfoUrl = [NSString stringWithFormat:@"%@%@",kApiBaseUrl,weatherUrl];
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    NSString *result = [cityInfoUrl stringByAddingPercentEncodingWithAllowedCharacters:set];

    [manager GET:result parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        if (completionBlock != nil) {
            completionBlock(responseObject, nil);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        if (error) {
            
        }
        if (completionBlock != nil) {
            completionBlock(nil, error);
        }
    }];
}
//--------------------------------------------------------------------------------------------------------------
#pragma mark Reachability Implementation
//--------------------------------------------------------------------------------------------------------------
- (void)reachabilityChanged:(NSNotification *)note
{
    Reachability* curReach = [note object];
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    [self updateInterfaceWithReachability:curReach];
}
//--------------------------------------------------------------------------------------------------------------
- (void)updateInterfaceWithReachability:(Reachability *)reachability
{
    NetworkStatus netStatus = [reachability currentReachabilityStatus];
    BOOL connectionRequired = [reachability connectionRequired];
    NSString* statusString = @"";
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    switch (netStatus)
    {
        case NotReachable:        {
            statusString = NSLocalizedString(@"Access Not Available", @"Text field text for access is not available");
            connectionRequired = NO;
            self.hasInternetConnection = NO;
            break;
        }
            
        case ReachableViaWWAN:        {
            statusString = NSLocalizedString(@"Reachable WWAN", @"");
            self.hasInternetConnection = YES;
            break;
        }
        case ReachableViaWiFi:        {
            statusString= NSLocalizedString(@"Reachable WiFi", @"");
            self.hasInternetConnection = YES;
            break;
        }
    }
    
}

@end
