//
//  WeatherInformationViewController.h
//  Weather
//
//  Created by Miki Dimitrov on 11/30/15.
//  Copyright © 2015 PovioLabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "City.h"
@interface WeatherInformationViewController : UIViewController

@property (nonatomic, retain) City *city; 
@property (weak, nonatomic) IBOutlet UILabel *lblCityName;
@property (weak, nonatomic) IBOutlet UILabel *lblTemperature;
@property (weak, nonatomic) IBOutlet UILabel *lblHumidity;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

@end
