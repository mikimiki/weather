//
//  ViewController.m
//  Weather
//
//  Created by Miki Dimitrov on 11/30/15.
//  Copyright © 2015 PovioLabs. All rights reserved.
//

#import "CityListViewController.h"
#import "WeatherInformationViewController.h"

@interface CityListViewController ()

@end

@implementation CityListViewController
//--------------------------------------------------------------------------------------------------------------
- (void)viewDidLoad {
    [super viewDidLoad];
    [self addRefreshControll];
}
//--------------------------------------------------------------------------------------------------------------
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    [self.tableView reloadData];
}
//--------------------------------------------------------------------------------------------------------------
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
//--------------------------------------------------------------------------------------------------------------
#pragma mark TableView-Delegate
//--------------------------------------------------------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([DataModel model].choosenCities.count > 0){
        return [DataModel model].choosenCities.count+1;
    }else{
        return 1;
    }
}
//--------------------------------------------------------------------------------------------------------------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    self.currentIndexPath = indexPath.row;

    if (indexPath.row == 0 && [DataModel model].choosenCities.count == 0) {
        return [self addCityTableViewcell:tableView];
    }else{
        if (indexPath.row == [DataModel model].choosenCities.count) {
            return [self addCityTableViewcell:tableView];
        }
        return [self cityTableViewcell:tableView];
    }
}
//--------------------------------------------------------------------------------------------------------------
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row==[DataModel model].choosenCities.count){
    [self performSegueWithIdentifier:@"AddCityViewController" sender:nil];
    }else{
    
        NSString *cityName = [[DataModel model].choosenCities objectAtIndex:indexPath.row];
        [[APIClient sharedClient] getWeatherInformationForCity:cityName andCompletionBlock:^(id object, NSError *error) {
            if (object) {
                City *selectedCity = [[DataModel model] parseCity:object andCityName:cityName];
                [self performSegueWithIdentifier:@"WeatherInformationViewController" sender:selectedCity];
            }
        }];

    }
}
//--------------------------------------------------------------------------------------------------------------
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Count %lu",(unsigned long)[DataModel model].choosenCities.count);

    if (indexPath.row == [DataModel model].choosenCities.count) {
        return NO;
    }
    return YES;
}
//--------------------------------------------------------------------------------------------------------------
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cityName = [[DataModel model].choosenCities objectAtIndex:indexPath.row];
    [[DataModel model] removeCityWithName:cityName];
    [tableView reloadData];
}
//--------------------------------------------------------------------------------------------------------------
#pragma mark CustomTableViewCell
-(CityTableViewCell *)cityTableViewcell:(UITableView *)tableView{
    static NSString*identifier = @"CityCustomCell";
    CityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[CityTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    NSString *cityName = [[DataModel model].choosenCities objectAtIndex:self.currentIndexPath];
    cell.lblCityName.text = cityName;
    [[APIClient sharedClient] getWeatherInformationForCity:cityName andCompletionBlock:^(id object, NSError *error) {
       City *currentCity = [[DataModel model] parseCity:object andCityName:cityName];
       double temp = [currentCity.temperature doubleValue];
       int celsius = temp-273.15;
       cell.lblCityTemperature.text = [NSString stringWithFormat:@"%d%@C",celsius,@"\u00B0"];
    }];
    return cell;
}
//--------------------------------------------------------------------------------------------------------------
-(AddCityTableViewCell *)addCityTableViewcell:(UITableView *)tableView{
    static NSString*identifier = @"AddCityCustomCell";
    AddCityTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[AddCityTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
    }
    return cell;
}
#pragma mark RefreshControll
//--------------------------------------------------------------------------------------------------------------
-(void)addRefreshControll{
    self.refreshControl = [[UIRefreshControl alloc]init];
    [self.tableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(refreshContent) forControlEvents:UIControlEventValueChanged];
}
//--------------------------------------------------------------------------------------------------------------
-(void)refreshContent{
    if ([self.refreshControl isRefreshing]) {
        [self.tableView reloadData];
        [self.refreshControl endRefreshing];
    }
}
//--------------------------------------------------------------------------------------------------------------
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"WeatherInformationViewController"]) {
        WeatherInformationViewController *controller =(WeatherInformationViewController*)[segue destinationViewController];
        controller.city = (City*)sender;
    }
}

@end
